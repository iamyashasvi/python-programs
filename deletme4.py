class Computer:

    def __init__(self):
        self.__maxprice = 900

    def sell(self):
        print("Selling price: {} ".format(self.__maxprice))

    def setMaxPrice(self,price):
        self.__maxprice = price

c = Computer()
c.sell()

#chaning the price
c.__maxprice = 1000
c.sell()

#usng setter function
c.setMaxPrice(7000)
c.sell()
