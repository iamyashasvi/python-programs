# Parent class
class Bird:

    def __init__(self):
        print("Bird is ready to fly ")

    def whoisThis(self):
        print(self.__class__)

    def swim(self):
        print("swim faster")

#child class
class Penguin(Bird):
    def __init__(self):
        #call super() function
        super().__init__()
        print("Penguin is ready to fly with Bird")

    def whoisThis(self):
        print("Penguin")

    def run(self):
        print("Run Faster")

peggy = Penguin()
peggy.whoisThis()
peggy.swim()
peggy.run()
