#deletme2.py
class Parrot:
    #class attributes
    species = "bird"
    #instance attributes
    def __init__(self,name="None",age=0):
        self.name = name
        self.age = age

    #instance method
    def sing(self,song):
        return "{} sings {} ".format(self.name,song)

    def dance(self):
        return "{} is now dancing ".format(self.name)

#instantiate the object
blu = Parrot("blu",12)

#call our instance methods
print(blu.sing("Happy"))
print(blu.dance())
