#deletme
class Parrot:
    #class attribute
    species = "bird"

    #instance attribute
    def __init__(self,name,age):
        self.name = name
        self.age = age

#instantiate the Parrot Class
blu = Parrot("Blue",20)
woo = Parrot(40,"woo")

#acess the class attributes
print("{} is a {} ".format(blu.name,blu.__class__.species))
print("{} is {} years old".format(woo.name,woo.age))
