#!/usr/bin/python3
class Person(object):
    #constructor
    def __init__(self,name):
        self.name = name

    #To get name
    def getName(self):
        return self.name

    #To check if this person is employee
    def isEmployee(self):
        return False


class Employee(Person):

    #here we return true
    def isEmployee(Self):
        return True

#Driver Code
emp = Person("Geek1") #An object of Person
print(emp.getName(),emp.isEmployee())

emp = Employee("Geeks2")  #AN object of Employee
print(emp.getName(),emp.isEmployee())
