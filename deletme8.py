class MyClass:
    #Hidden member of MyClass
    __hiddenVariable = 0

    #A member methd that changes
    #__hiddenVariable

    def add(self,increment):
        self.__hiddenVariable += increment
        print(self.__hiddenVariable)

myObject = MyClass()
myObject.add(2)
myObject.add(8)

#This line cause error
print(myObject.__hiddenVariable)
