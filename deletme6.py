class Person:
    #init method or constructor
    def __init__(self,name):
        self.name = name

    def say_hi(self):
        print("hello, my name is ",self.name)

p = Person(str(input("Hi, enter your name ")))
p.say_hi()
