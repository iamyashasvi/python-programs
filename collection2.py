import collections
numshoes = int(input().split())
shoes = collections.Counter(map(int,input().split()))
numcust = int(input().split())

income = 0

for i range(numcust):
    size, price = map(int,input().split())
    if shoes[size]:
        income += price
        shoes[size] -= 1

print(income)
